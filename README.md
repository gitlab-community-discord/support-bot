# GitLab Support Discord Bot

This is the source code where the unofficial Discord bot for GitLab Support in [the unofficial Discord server][join-server].

[join-server]: https://discord.gg/bft7b82td6

## Setup

The bot is written in JavaScript, so you need to install Node.js and the dependencies we need for running the bot.

1. Clone the Git repository with 
```sh
## Clone from the main repo
git clone https://gitlab.com/GitLab-Community-Discord/GitLab-Support-DiscordBot.git
```

## Contributing

See the `CONTRIBUTING.md` file for details.

## License

MIT